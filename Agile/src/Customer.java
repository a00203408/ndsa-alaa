public class Customer {
	public GUI g;
	private Database db;
	public void connectToGUI(GUI g){
		this.g=g;
	}
	public Customer(){
		db=new Database();
	}
	public String validate(String firstname,String lastname,String birthyear, 
			String address,String phone, String email,String areacode){
		String answer ="";
	//	Calendar now=Calendar.getInstance();
	//	int year=now.get(Calendar.YEAR);
		if(firstname.equals("")||firstname==""){
			answer+= "The firstname is null. \n";
		}
		else if(lastname.equals("")||lastname==""){
			answer +="The lastname is null . \n";
		}
		else if(birthyear.equals("")||birthyear==""){
			answer+= "The birthyear is null. \n";
		}
		else if(address.equals("")||address==""){
			answer+= "The address is null. \n";
		}
		else if(areacode.equals("")||areacode==""){
			answer+= "The areacode is wrong. \n";
		}
		else if(phone.length()!=10||(!phone.matches("[0-9]+"))){
			if(!phone.equals("")||phone!="")
				answer+="The phone number is wrong. \n";
		}
		else if(email.indexOf(".com")==-1||email.indexOf("@")==-1){
			if(email.equals("")||email=="")
				answer+= "The E-mail is null. \n";
			else
				answer+= "The E-mail is wrong. \n";
		}
		return answer;
	}
	public void Create_Customer(String firstname,String lastname,String birthyear, String address,
			String phone, String email,String areacode){
		String answer=validate(firstname,lastname,birthyear, address,phone,email,areacode);	
		if(answer==""){
			boolean result=db.addNewCustomer(firstname, lastname,address, email, phone);
			g.createNewCustomer("OK");
			}
		else
			g.createNewCustomer(answer);
	}
	public void Update_Customer(String customerid,String firstname,String lastname,String birthyear, String address,
			String phone, String email,String areacode){
		String answer=validate(firstname,lastname,birthyear, address,phone,email,areacode);	
		if(answer==""){
			boolean result=db.updateCustomer(customerid,firstname, lastname,address, email, phone);
			g.createNewCustomer("OK");
			}
		else
			g.createNewCustomer(answer);
	}
	public void Search_Customer(String name,String customerid, String subid,String address){
		String answer = "";
		String type="";
		if(name.equals("")||name==""){
			answer+= "The customer name is null. \n";
		}
		else if(customerid.equals("")||customerid==""){
			answer +="The customer id is null . \n";
		}
		else if(subid.equals("")||subid==""){
			answer+= "The subcripiton id is null. \n";
		}
		else if(address.equals("")||address==""){
			answer+= "The address is null. \n";
		}
		boolean result =db.searchCustomer(type);
		if(answer==""){
			g.createNewCustomer("OK");
			}
		else
			g.createNewCustomer(answer);
	}
	public void Achieve_Customer(String customerid){
		boolean result=db.deleteCustomer(customerid);
	}
}


